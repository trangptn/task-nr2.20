const mongoose = require('mongoose');
const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');

const createOrder = (req, res) => {
    //B1:Thu thap du lieu
    const {
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status,
        userId } = req.body;
    //B2:Validate data
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status.json({
            "status": "Bad request",
            "message": "User Id is not valid"
        })
    }
    //B3:Xu ly va tra ve ket qua
    let newOrder = new orderModel({
        _id: new mongoose.Types.ObjectId(),
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    })

    orderModel.create(newOrder)
        .then((data) => {
            userModel.findByIdAndUpdate(userId, {
                $push: { oders: data._id }
            })
            .then((success) => {
                res.status(201).json({
                    message: "Successfully created",
                    result:data
                })
            })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err.message}`
                    })
                })
    })
}

const getAllOrder=(req,res) =>{
    //B1: Thu thap du lieu
    //B2:validate data
    //B3: Xu ly va tra ve ket qua
    orderModel.find().exec()
        .then(data=> {
            return res.status(200).json({
                message:"Successfully load all data!",
                oder: data
            })
        })
        .catch(error =>{
            return res.status(500).json({
                message:`Internal sever error: ${error.message}`
            })
        })
}
const getOrderById=(req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
   //B3: Xu ly va tra ve ket qua
   orderModel.findById(orderId)
       .then(data=>{
           if(!data)
           {
               return res.status(404).json({
                   message:" Order not found"
               })
           }
          return res.status(200).json({
           message:"Successfully load data by ID!",
           order: data
          })
       })
       .catch(err=>{
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       })
}
const updateOrderById= async (req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
    const {
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status} = req.body;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
 
   //B3: Xu ly va tra ve ket qua
  try{
    let updateOrder={
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    }
    const updateO= await orderModel.findByIdAndUpdate(orderId,updateOrder);
    if(updateO){
        return res.status(200).json({
            message:"Successfully update data by ID!",
            data: updateO
    })
  }
  else{
        return res.status(404).json({
        message:" Order not found"
        })
  }
}catch(err){
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       }
}
const deleteOrderById= async (req,res)=>{
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    var userId=req.query.userId;


    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Order ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const deleteOrder= await  orderModel.findByIdAndDelete(orderId)
        if(userId != undefined){
          await  userModel.findByIdAndUpdate(userId,{
                $pull: {orders: orderId}})
        }

        if(deleteOrder){
            return res.status(200).json({
                message:"Successfully delete data by ID!",
                data: deleteOrder
               })
        }
        else{
            return res.status(404).json({
                message:" Order not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
        message:`Internal sever error: ${error.message}`
        })
    }
}
module.exports={
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}